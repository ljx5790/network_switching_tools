﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml;

namespace NetworkSwitch
{
    public class AppConfig
    {
        /// <summary>
        /// 内网网卡的网关
        /// </summary>
        public static string LocalNetWork
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("LocalNetWork");
            }
        }

        /// <summary>
        /// 第二网卡的网关（一般是外网）
        /// </summary>
        public static string Internet
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("Internet");
            }
        }

        /// <summary>
        /// 第二网卡（一般是外网）的网络适配器
        /// </summary>
        public static string NetworkAdapter
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("NetworkAdapter");
            }
        }


        /// <summary>
        /// 当前网络状态  1:仅内网  2：内外双网络
        /// </summary>
        public static int NetworkType
        {
            get
            {
                return Convert.ToInt32(GetAppConfig("NetworkType"));
            }
            set
            {
                SetAppConfig("NetworkType", value.ToString());
            }
        }

        public static void SetAppConfig(string appKey, string appValue)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(System.Windows.Forms.Application.ExecutablePath + ".config");

            var xNode = xDoc.SelectSingleNode("//appSettings");

            var xElem = (XmlElement)xNode.SelectSingleNode("//add[@key='" + appKey + "']");
            if (xElem != null) xElem.SetAttribute("value", appValue);
            else
            {
                var xNewElem = xDoc.CreateElement("add");
                xNewElem.SetAttribute("key", appKey);
                xNewElem.SetAttribute("value", appValue);
                xNode.AppendChild(xNewElem);
            }
            xDoc.Save(System.Windows.Forms.Application.ExecutablePath + ".config");
        }

        public static string GetAppConfig(string appKey)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(System.Windows.Forms.Application.ExecutablePath + ".config");

            var xNode = xDoc.SelectSingleNode("//appSettings");

            var xElem = (XmlElement)xNode.SelectSingleNode("//add[@key='" + appKey + "']");

            if (xElem != null)
            {
                return xElem.Attributes["value"].Value;
            }
            return string.Empty;
        }
    }
}
